const {
    createGame,
    gameList,
    getRoom,
    updatePlayerChoice,
    updateWinner,
} = require("../game.repo");

describe("game.repo", () => {
    describe("createGame test", () => {
        it('should return create new game room', async() => {
            const testDataRoom = {
                name: "room 1",
                description: "description room 1"
            }
            const result = await createGame(testDataRoom);
            console.log(result);
            expect(result.name).toBe("room 1");
            expect(result.description).toBe("description room 1");
            expect(result.winner).toBe(null);
        });
    });
    describe("gameList test", () => {
        it('should return data all game room list sort by room no winner DESC', async() => {
            const result = await gameList(pageNumber = undefined)
            expect(result).toBeTruthy();
        });
        it('should return data all game room list sort by new room DESC', async() => {
            const result = await gameList(pageNumber = 1);
            expect(result).toBeTruthy();
        });
    });
    describe("getRoom test", () => {
        it('should return room by id selected', async() => {
            const result = await getRoom({ roomId: 1 })
            expect(result.id).toBe(1);
        });
    });
    describe("updatePlayerChoice", () => {
        it('should return where first player (1) choice', async() => {
            const result = await updatePlayerChoice({
                player: "player1",
                userId: 1,
                userChoice: "S",
                roomId: 1
            });

            expect(result).toBeTruthy();
            expect(result[1][0].user1_choice).toBe('S');
        });
        it('should return where second player (2) choice', async () => {
            const result = await updatePlayerChoice({
                player: "player2",
                userId: 2,
                userChoice: "R",
                roomId: 1
            });
            expect(result).toBeTruthy();
            expect(result[1][0].user2_choice).toBe('R');
        });
    });
    describe("updateWinner", () => {
        it('should return update room winner', async() => {
            const result = await updateWinner({roomId : 1, result: 'Gue Winner'})
            console.log(result);
            expect(result).toBeTruthy();
            expect(result[0]).toBe(1);
            expect(result[1][0].winner).toBe('Gue Winner');
        });
    })
})