const { createUser, checkEmailAllUser, getAllUser, editUser } = require("../user.repo");
const { faker } = require("@faker-js/faker");
const { User } = require("../../database/models");

const testData = {
    fullname: "Arif Setiawan",
    email: "ariefroyal@gmail.com",
    password: "Password@123!",
};

describe("user.repo.test", () => {

    describe("test getAllUser", () => {
        it('should get all user list', async() => {
            const result = await getAllUser({ pageNumber: 1, limitParm: 10 });
            
            expect(result).toBeTruthy();
        });
        it('should get all user without limitParm', async() => {
            const result = await getAllUser({pageNumber:1});

            expect(result.length).not.toBe(0);
            expect(result).toBeTruthy();
        });
        it('should get all user without pageNumber', async() => {
            const result = await getAllUser({pageNumber: undefined});

            expect(result.length).not.toBe(0);
            expect(result).toBeTruthy();
        });
    });

    describe("test createUser function", () => {
        it('should create new user', async () => {
            const result = await createUser(testData);
            console.log(result);
            expect(result.fullname).toBe(testData.fullname);
            expect(result.email).toBe(testData.email);
            expect(result.password).toBe(testData.password);
        });
    });
    describe("test checkEmailAllUser", () => {
        it("should return where email existed", async () => {
            const result = await checkEmailAllUser(testData.email);
            console.log(result);
            expect(result).toBeTruthy();
        });
        it('should return where email not exist', async() => {
            const result = await checkEmailAllUser("newmail@gmail.com")
            expect(result).toBeFalsy();
        });
    });

    describe("test editUser", () => {
        it("should edit user data email and fullname", async() => {
            const dataUserEdit = {
                fullname: "Full Name Edit 1",
                email: "emailedieditt1@gmail.com",
                userId: 1
            }
            const result = await editUser(dataUserEdit);

            expect(result).toStrictEqual([1]);
            // expect(result).toBeTruthy();
        });
    })
});