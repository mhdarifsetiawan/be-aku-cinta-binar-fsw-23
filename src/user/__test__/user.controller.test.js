const { faker } = require("@faker-js/faker");
const httpMocks = require('node-mocks-http');
const { createUser } = require('../user.controller');
const userService = require("../user.service");

const req = httpMocks.createRequest();
const res = httpMocks.createResponse();

userService.createUser = jest.fn();
describe("userController", () => {
    describe("test createUser", () => {
        it('should return created new user', async() => {
            const newUser = {
                fullname: faker.name.fullName(),
                email: faker.internet.email(),
                password: faker.random.word(),
            }
            req.body = newUser;
            userService.createUser.mockReturnValue({ newUser });

            const result = await createUser(req, res)

            expect(userService.createUser).toBeCalled();

            expect(result.statusCode).toBe(200);
            expect(result._getJSONData()).toStrictEqual({
                message: "Congratulations! Your account has been created",
            });
        });
    })
})