const {
    userAddScore,
    createUser,
    getAllUser,
    editUser,
    getUserById,
    editPassword,
    findEmail
} = require("../user.service");
const bcrypt = require("bcrypt");
const { faker } = require("@faker-js/faker");
const userRepo = require("../user.repo");

const testData = {
    fullname: faker.name.fullName(),
    email: faker.internet.email(),
    password: faker.internet.password(),
};

bcrypt.hash = jest.fn(() => "test");

describe("userService test", () => {
    describe("getAllUser", () => {
        it('should return all data user from database', async() => {
            userRepo.getAllUser = jest.fn(() => [testData]);
            const result = await getAllUser({ pageNumber: 1, limitParm: 10 });
            console.log(result);
            expect(result[0].fullname).toBe(testData.fullname);
        });
    });
    describe("createUser", () => {
        it('should create a user', async() => {
            //given
            userRepo.checkEmailAllUser = jest.fn(() => null);

            //when
            const result = await createUser(testData);

            // result
            // apakah bcrypt.hash sudah dipanggil?
            expect(bcrypt.hash).toBeCalledWith(testData.password, 10);
            // apakah userRepo.checkEmilAllUser dipanggil?
            expect(userRepo.checkEmailAllUser).toBeCalledWith(testData.email);
            expect(result.fullname).toBe(testData.fullname);
            expect(result.email).toBe(testData.email);
        });
        it("should return null because the email already registered", async () => {
            // given / kondisinya
            userRepo.checkEmailAllUser = jest.fn(() => {
            return {
                fullName: "Sudah terdaftar",
                email: "email@sudahterdaftar.com",
                };
            });
            // when
            const result = await createUser(testData);
            // result / expect
            expect(bcrypt.hash).toBeCalledWith(testData.password, 10);
            expect(userRepo.checkEmailAllUser).toBeCalledWith(testData.email);
            expect(result).toBeNull();
        });
    });
    describe("editUser test", () => {
        const dataUserEdit = {
            fullname: "Full Name Edit 1",
            email: "emailedieditt1@gmail.com",
            userId: 1,
            authUserId: 1,
        };
        it('should return update data user success', async() => {
            // give
            userRepo.checkEmailAllUser = jest.fn(() => null);
            // when
            const result = await editUser(dataUserEdit);
            // result
            expect(result).toBe("Update successful");
        });
        it('should return where email already used other user', async() => {
            userRepo.checkEmailAllUser = jest.fn(() => {
                return {
                    fullName: "Full Name Terdaftar",
                    email: "emailterdaftar@gmail.com",
                };
            });
            userRepo.checkSameEmail = jest.fn(() => null);
            const result = await editUser(dataUserEdit);

            expect(result).toBe(
                "This email is already being used, Please choose another email"
            );
        });
    });
});